<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email every week on Tuesday at 10:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::raw('Hope you are pretty cool!', function ($message) {
            $message->from('addressbook@gmail.com', 'Address Book');

            $message->to('sunday.esho@crowngas.co.uk')->cc('jessunesho@yahoo.com');
        });
    }
}
