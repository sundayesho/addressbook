<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personsdetail;
use App\Models\Personsaddress;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Controllers\Controller;
use App\Mail\AddressAdded;
use App\Mail\AddressUpdated;
use App\Mail\AddressDeleted;

class PersonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
        return view('persons.admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $data = Personsdetail::with('personsaddress')->get();

        return view('persons.home');
    }

    public function address()
    {
        $data = Personsdetail::with('personsaddress')->get();
        return Datatables::collection($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('persons.create');
    }

    public function sendEmail()
    {
        $email = \Mail::to('jessunesho@yahoo.com');

        return $email;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatePerson($request);

        $personsdetail = new Personsdetail();
        $personsdetail->first_name = request('first_name');
        $personsdetail->last_name = request('last_name');
        $personsdetail->save();

        $personsaddress = new Personsaddress();
        $personsaddress->person_id = $personsdetail->id;
        $personsaddress->address1 = request('address1');
        $personsaddress->address2 = request('address2');
        $personsaddress->city = request('city');
        $personsaddress->county = request('county');
        $personsaddress->postcode = request('postcode');
        $personsaddress->telephone = request('telephone');
        $personsaddress->save();

        $this->sendEmail()->send(
            new AddressAdded($personsdetail)
        );

        return redirect('persons/home')->with('message', 'A new Address has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->query($id);
        return view('persons.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->query($id);
        return view('persons.edit', compact('data'));
    }

    private function query($id = null)
    {
        $query = ($id ? Personsdetail::find($id) : Personsdetail::all());

        return $query;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function update(Request $request, $id)
    public function update(Request $request, $id)
    {
        $this->validatePerson();
        $updateAddress = $this->query($id);

        $updateAddress->first_name = request('first_name');
        $updateAddress->last_name = request('last_name');
        $updateAddress->personsaddress->email = request('email');
        $updateAddress->personsaddress->address1 = request('address1');
        $updateAddress->personsaddress->address2 = request('address2');
        $updateAddress->personsaddress->city = request('city');
        $updateAddress->personsaddress->county = request('county');
        $updateAddress->personsaddress->postcode = request('postcode');
        $updateAddress->personsaddress->telephone = request('telephone');

        $updateAddress->save();
        $updateAddress->personsaddress->save();

        $this->sendEmail()->send(
            new AddressUpdated($updateAddress)
        );

        return redirect('persons/home')->with('message', 'Record Update was Successful!');
    }

    public function deleteAddressConfirm(Personsdetail $deleteAddress)
    {
        return response()->json([
            'html' => view('modals.confirmDeleteAddress', [
                'personsDetail' => $deleteAddress,
            ])->render()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personsdetail $deleteAddress)
    {
        $deleteAddress->delete();

        $this->sendEmail()->send(
            new AddressDeleted($deleteAddress)
        );

        return response()->json(null, 204);
    }

    private function validatePerson()
    {
        return request()->validate([
          'first_name' => 'required|min:2',
          'last_name' => 'required|max:225',
          'email' => 'required',
          'address1' => 'required',
          'address2' => 'nullable',
          'city' => 'required',
          'county' => 'nullable',
          'postcode' => 'required',
          'telephone' => 'required|regex:/(0)[0-9]{10}/',
      ]);
    }
}
