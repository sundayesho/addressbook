<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**Admin.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->is_admin == 1) {
            return $next($request);
        }
        return redirect('persons/home')->with('message', 'You have no admin access');
    }
}
