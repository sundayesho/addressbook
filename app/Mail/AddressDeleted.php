<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddressDeleted extends Mailable
{
    use Queueable, SerializesModels;

    public $deleteAddress;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($deleteAddress)
    {
        $this->deleteAddress = $deleteAddress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.address-deleted');
    }
}
