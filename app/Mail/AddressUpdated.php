<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddressUpdated extends Mailable
{
    use Queueable, SerializesModels;

    public $updateAddress;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($updateAddress)
    {
        $this->updateAddress = $updateAddress;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.address-updated');
    }
}
