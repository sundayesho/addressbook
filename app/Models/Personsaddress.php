<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personsaddress extends Model
{
    use SoftDeletes;
    protected $table = 'tblpersonsaddresses';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $guarded = ['id', '_token'];

    public function personsdetail()
    {
        return $this->belongsTo(Personsdetail::class);
    }
    /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
}
