<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personsdetail extends Model
{
    use SoftDeletes;
    protected $table = 'tblpersonsdetails';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $guarded = ['id', '_token'];

    public function personsaddress()
    {
        return $this->hasOne(Personsaddress::class, 'person_id');
    }
    /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
}
