<?php

use Illuminate\Database\Seeder;
use App\Models\Personsaddress;

class PersonsaddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $personsAddresses = [
        [
          'person_id' => 1,
          'address1' => '34',
          'address2' => 'Durchy Road',
          'city' => 'Salford',
          'county' => 'Greater Manchester',
          'postcode' => 'M4 5HG',
          'telephone' => '01111111111',
          'email' => 'sunday.esho@crowngas.co.uk'
        ],
        [
          'person_id' => 2,
          'address1' => '34',
          'address2' => 'Manson Cresent',
          'city' => 'Salford',
          'county' => 'Greater Manchester',
          'postcode' => 'M5 6HF',
          'telephone' => '02222222222',
          'email' => 'john.smith@crowngas.co.uk'
        ],
        [
          'person_id' => 3,
          'address1' => '56',
          'address2' => 'Hope Street',
          'city' => 'Salford',
          'county' => 'Greater Manchester',
          'postcode' => 'M7 6YU',
          'telephone' => '03333333333',
          'email' => 'adam.clifford@crowngas.co.uk'
        ],
        [
          'person_id' => 4,
          'address1' => '13',
          'address2' => 'Queen Street',
          'city' => 'Bury',
          'county' => 'Greater Manchester',
          'postcode' => 'BL7 6BF',
          'telephone' => '04444444444',
          'email' => 'jon.wood@crowngas.co.uk'
        ],
        [
          'person_id' => 5,
          'address1' => '72',
          'address2' => 'Jameson Road',
          'city' => 'Bury',
          'county' => 'Greater Manchester',
          'postcode' => 'BL7 7AD',
          'telephone' => '05555555555',
          'email' => 'adewale.david@crowngas.co.uk'
        ],
        [
          'person_id' => 6,
          'address1' => '123',
          'address2' => 'Goodwill Avenue',
          'city' => 'Salford',
          'county' => 'Greater Manchester',
          'postcode' => 'M6 8HJ',
          'telephone' => '06666666666',
          'email' => 'liam.ashwood@crowngas.co.uk'
        ],
      ];
      //insert all $personsAddresses
      foreach ($personsAddresses as $personsAddress) {
        Personsaddress::firstOrCreate([
          'person_id' => $personsAddress['person_id'],
          'address1' => $personsAddress['address1'],
          'address2' => $personsAddress['address2'],
          'city' => $personsAddress['city'],
          'county' => $personsAddress['county'],
          'postcode' => $personsAddress['postcode'],
          'telephone' => $personsAddress['telephone'],
          'email' => $personsAddress['email']
        ]);
      }
    }
}
