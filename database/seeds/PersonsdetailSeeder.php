<?php

use Illuminate\Database\Seeder;
use App\Models\Personsdetail;

class PersonsdetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $personsDetails = [
        [
          'first_name' => 'Sunday',
          'last_name' => 'Esho'
        ],
        [
          'first_name' => 'John',
          'last_name' => 'Smith'
        ],
        [
          'first_name' => 'Adam',
          'last_name' => 'Clifford'
        ],
        [
          'first_name' => 'Jon',
          'last_name' => 'Wood'
        ],
        [
          'first_name' => 'Adewale',
          'last_name' => 'David'
        ],
        [
          'first_name' => 'Liam',
          'last_name' => 'Ashwood'
        ]
      ];
      //insert all $personsDetails
      foreach ($personsDetails as $personsDetail) {
        Personsdetail::firstOrCreate([
          'first_name' => $personsDetail['first_name'],
          'last_name' => $personsDetail['last_name']
        ]);
      }
    }
}
