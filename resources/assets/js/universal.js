let personData = [
    {data: 'id'},
    {data: 'first_name'},
    {data: 'last_name'},
    {data: 'personsaddress.email'},
    {data: 'personsaddress.telephone'},
    {
        data: null,
        render: function(data, type, row, meta) {
            return '<a href='+ "/persons/{person}".replace('{person}', row.id) +'><button class="btn btn-primary btn-sm"><i class="fa fa-fw fa-eye"></i> View</button></a>';
        }
    },
    {
        data: null,
        render: function(data, type, row, meta) {
            return '<a href='+ "/persons/{person}/edit".replace('{person}', row.id) +'><button class="btn btn-primary btn-sm"><i class="fa fa-fw fa-edit"></i> Edit</button></a>';
        }
    },
    {
        data: null,
        render: function(data, type, row, meta) {
            return '<button class="btn btn-danger btn-sm deletePersonDetailsModal" name="deletePersonDetailsButtonModal" data-id = "' + row.id + '" ><i class="fa fa-fw fa-trash-o"></i> Delete</button>';
        }
    },
];

$(document).ready(function(){

    let personDetails = $('#personTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 15,
        ajax: '/persons/address',
        columns: personData,
        autoWidth: false,
        language: {
            processing: loadingSpinner + 'Loading records.........'
        },

        order: [[ 0, "DESC" ]],
    });

    $(document).delegate('button[name=deletePersonDetailsButtonModal]', 'click', function() {
            button = $(this);
            var id = button.attr('data-id');

        $.ajax({
                url: '/' + 'persons/{deleteAddress}/delete/confirm'.replace('{deleteAddress}', id),
                method: 'GET',
                dataType: 'json',
                beforeSend: function () {
                    $('#confirmDeleteAddress').find('.modal-content').empty();
                    $('#confirmDeleteAddress').modal('show').find('.modal-content').html(loadingHtml);
                },
                success: function (response) {
                    $('#confirmDeleteAddress').find('.modal-content').empty();
                    $('#confirmDeleteAddress').find('.modal-content').html(response.html);
                },
                error: function () {
                    $('.confirmDeleteAddress').html('<div class="alert alert-danger">There was a problem deleting this record, Please Contact Customer Service</div>');
                },
                complete: function () {
                    button.find('i').removeClass('fa-spin fa-spinner').addClass('fa-delete');
                    $('.selectpicker').selectpicker();
                }
            });
        });

        $(document).delegate('.deleteAddressButton', 'click', function(e) {
            e.preventDefault()
            var button = $(this);
            var id = button.attr('data-id');

            $.ajax({
                url: '/' + 'persons/{deleteAddress}/delete'.replace('{deleteAddress}', id),
                method: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    button.find('i').removeClass('fa-trash-o').addClass('fa-spin fa-spinner');
                },
                success: function() {
                    $('#confirmDeleteAddress').modal('toggle');
                    personDetails.ajax.reload();
                    $('.panel-body').prepend('<div class="alert alert-success">Record deleted successfully</div>');
                    $('.alert-success').fadeOut(30000);
                },
                error: function(response) {
                    $('.panel-body').prepend('<div class="alert alert-danger">There was an error removing address. Please Contact Customer Service.</div>');
                    button.find('i').removeClass('fa-spin fa-spinner').addClass('fa-trash-o');
                }
            });
        });

});
