@component('mail::message')
# New Address Added

{{ ($personsdetail->first_name . ' ' . $personsdetail->last_name . " has been added to the Address Book.") }}

@component('mail::button', ['url' => url('/persons/' . $personsdetail->id)])
View Address
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
