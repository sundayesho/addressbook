@component('mail::message')
# Record Deletion

{{ ($deleteAddress->first_name . ' ' . $deleteAddress->last_name . " record has been deleted from our database.") }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
