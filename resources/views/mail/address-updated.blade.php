@component('mail::message')
# Record Update

{{ ($updateAddress->first_name . ' ' . $updateAddress->last_name . " details has been updated.") }}

@component('mail::button', ['url' => url('/persons/' . $updateAddress->id)])
View Details
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
