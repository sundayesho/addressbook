<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Are you sure?</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    </div>
    <div class="modal-body">
        <p>Do you really want to remove this address? This process cannot be undone!</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal"> No, Cancel</button>
        <button name="deleteAddressButton" type="button" data-id="{{ $personsDetail->id}}" class="btn btn-danger deleteAddressButton"> <i class="fa fa-fw fa-trash-o"></i> Yes, Delete</button>
    </div>
</div>
