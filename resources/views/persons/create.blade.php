@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>Register New Address</strong></div></br>
                <body>
                  <form method = "POST" action = "/persons/store">
                      @csrf
                      @include('layouts.errors')
                      <div class="form-group">
                        <label for="title">First Name</label>
                        <input type="text" name="first_name" id="first_name" class="input {{ $errors->has('first_name') ? 'is-danger' : '' }} form-control" value = "{{ old('first_name') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Last Name</label>
                        <input type="text" name="last_name" id="last_name" class="input {{ $errors->has('last_name') ? 'is-danger' : '' }} form-control" value = "{{ old('last_name') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Email</label>
                        <input type="text" name="email" id="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }} form-control" value = "{{ old('email') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 1</label>
                        <input type="text" name="address1" id="address1" class="input {{ $errors->has('address1') ? 'is-danger' : '' }} form-control" value = "{{ old('address1') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 2</label>
                        <input type="text" name="address2" id="address2" class="form-control" value = "{{ old('address2') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">City</label>
                        <input type="text" name="city" id="city" class="input {{ $errors->has('city') ? 'is-danger' : '' }} form-control" value = "{{ old('city') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">County</label>
                        <input type="text" name="county" id="county" class="form-control" value = "{{ old('county') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Postcode</label>
                        <input type="text" name="postcode" id="postcode" class="input {{ $errors->has('postcode') ? 'is-danger' : '' }} form-control" value = "{{ old('postcode') }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Telephone</label>
                        <input type="text" name="telephone" id="telephone" class="input {{ $errors->has('telephone') ? 'is-danger' : '' }} form-control" value = "{{ old('telephone') }}">
                      </div>
                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                  </form>
                </body>
            </div>
        </div>
    </div>
</div>
@endsection
