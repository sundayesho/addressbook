@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>Edit Details</strong></div></br>
                <body>
                  <form method = "POST" action = "/persons/{{ $data->id }}">
                    @method('PATCH')
                    @csrf
                    @include('layouts.errors')
                      <div class="form-group">
                        <label for="title">First Name</label>
                        <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $data->first_name }}" required>
                      </div>
                      <div class="form-group">
                        <label for="title">Last Name</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $data->last_name }}" required>
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 1</label>
                        <input type="text" name="address1" id="address1" class="form-control" value="{{ $data->personsaddress->address1 }}" required>
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 2</label>
                        <input type="text" name="address2" id="address2" class="form-control" value="{{ $data->personsaddress->address2 }}">
                      </div>
                      <div class="form-group">
                        <label for="title">City</label>
                        <input type="text" name="city" id="city" class="form-control" value="{{ $data->personsaddress->city }}" required>
                      </div>
                      <div class="form-group">
                        <label for="title">County</label>
                        <input type="text" name="county" id="county" class="form-control" value="{{ $data->personsaddress->county }}">
                      </div>
                      <div class="form-group">
                        <label for="title">Postcode</label>
                        <input type="text" name="postcode" id="postcode" class="form-control" value="{{ $data->personsaddress->postcode }}" required>
                      </div>
                      <div class="form-group">
                        <label for="title">Telephone</label>
                        <input type="text" name="telephone" id="telephone" class="form-control" value="{{ $data->personsaddress->telephone }}" required>
                      </div>
                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Update Details</button>
                      </div>
                  </form>
                </body>
            </div>
        </div>
    </div>
</div>
@endsection
