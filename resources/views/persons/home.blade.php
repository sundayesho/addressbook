@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="panel-heading text-right">
        <div class="text-center">
            <a href="{{url('admin/routes')}}"><strong>Admin</strong></a>
        </div>
        <p><a href="{{ route('persons.create') }}" class="btn btn-success"><i class="fa fa-fw fa-user-plus"></i>Add New Address</a></p>
    </div>
    <div class='panel-body'>
        <table width="100%" id="personTable" class="table table-striped table-valign table-condensed table-bordered">
          @if (session('message'))
            <p class="alert alert-danger">{{ session('message') }}</p>
          @endif
          <thead>
              <th class="th-sm">Identity Number</th>
              <th class="th-sm">First Name</th>
              <th class="th-sm">Surname Name</th>
              <th class="th-sm">Email</th>
              <th class="th-sm">Telephone</th>
              <th class="th-sm"></th>
              <th class="th-sm"></th>
              <th class="th-sm"></th>
            </tr>
          </thead>
        </table>
    </div>
  </div>

  <div class="modal fade" id="confirmDeleteAddress" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-confirm">
          <div class="modal-content">
          </div>
      </div>
  </div>
@endsection
