@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>Person's Details</strong></div></br>
                <body>
                      <div>
                        <label for="title">First Name</label>
                        <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $data->first_name }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Surname</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $data->last_name }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $data->personsaddress->email }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 1</label>
                        <input type="text" name="address1" id="address1" class="form-control" value="{{ $data->personsaddress->address1 }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Address Line 2</label>
                        <input type="text" name="address2" id="address2" class="form-control" value="{{ $data->personsaddress->address2 }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">City</label>
                        <input type="text" name="city" id="city" class="form-control" value="{{ $data->personsaddress->city }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">County</label>
                        <input type="text" name="county" id="county" class="form-control" value="{{ $data->personsaddress->county }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Postcode</label>
                        <input type="text" name="postcode" id="postcode" class="form-control" value="{{ $data->personsaddress->postcode }}" readonly>
                      </div>
                      <div class="form-group">
                        <label for="title">Telephone</label>
                        <input type="text" name="telephone" id="telephone" class="form-control" value="{{ $data->personsaddress->telephone }}" readonly>
                      </div>
                </body>
            </div>
        </div>
    </div>
</div>
@endsection
