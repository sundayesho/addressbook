<?php

/*
|-------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('admin/routes', 'PersonController@admin')->middleware('admin');
Auth::routes();

Route::group(['prefix' => 'persons', 'as' => 'persons.'], function () {
    Route::get('home', 'PersonController@home')->name('home');
    Route::get('address', 'PersonController@address')->name('addresses');
    Route::get('create', 'PersonController@create')->name('create');
    Route::post('store', 'PersonController@store')->name('store');
    Route::get('{person}', 'PersonController@show')->name('view');
    Route::get('{person}/edit', 'PersonController@edit')->name('edit');
    Route::patch('{id}', 'PersonController@update')->name('update');
    Route::get('{deleteAddress}/delete', 'PersonController@destroy')->name('delete');
    Route::get('{deleteAddress}/delete/confirm', 'PersonController@deleteAddressConfirm')->name('delete.address.confirm');
});
