const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts/bootstrap');
mix.copy('node_modules/jquery-contextmenu/dist/font', 'public/css/font');
mix.copy('node_modules/jquery-ui-dist/images', 'public/css/images');

mix.scripts([
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/jquery-ui-dist/jquery-ui.min.js',
  'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
  'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
  'node_modules/bootstrap-toggle/js/bootstrap-toggle.min.js',
  'node_modules/jquery-contextmenu/dist/jquery.contextMenu.js',
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/datatables.net/js/jquery.dataTables.min.js',
  'node_modules/datatables.net/js/jquery.dataTables.js',
  'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
  'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
  'node_modules/autosize/dist/autosize.js',
  'resources/assets/js/app.js',
], 'public/js/app.js').version();
mix.js('resources/assets/js/universal.js', 'public/js/universal.js').version();

mix.sass('resources/assets/sass/app.scss', 'public/css/app.css').version();
